# CRUD DE PEDIDOS Y PRODUCTOS

## Descripción

Backend del sistema

## Requerimientos del sistema

- NodeJS v16
- Postgres 14

## Instalación y configuración

Dentro de la carpeta del proyecto, instalar las dependencias con:
```bash
$ npm install
```

Configurar los archivos de conexión a la base de datos ./ormconfig.ts y ./src/app.module.ts con las credenciales respectivas de postgres (Se intento usar variables de entorno pero hubo problemas en las detección en archivo .env)

Una vez configurada la conexión a la base de datos ejecutar las migraciones y seeders con:
```bash
$ npm run typeorm:run-migrations
```

Por último levantar el servidor con:
```bash
$ npm start
```
