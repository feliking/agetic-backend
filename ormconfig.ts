import { DataSource } from "typeorm";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";

const options: PostgresConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'agetic',
  entities: ['dist/src/**/*.entity{.ts,.js}'],
  synchronize: false,
  logging: true,
  migrations: ['dist/src/db/migrations/**/*{.ts,.js}'],
}

export default new DataSource(options)