# Backend
FROM node:16

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run typeorm:run-migrations

RUN npm run build

CMD [ "node", "dist/main.js" ]