import { Pedido } from "src/pedidos/entities/pedido.entity";
import { Producto } from "src/productos/entities/producto.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('pedido_producto')
export class PedidoProducto {
    @PrimaryGeneratedColumn()
    public id: number

    @Column()
    public pedidoId!: number

    @Column()
    public productoId!: number

    @Column()
    public quantity: number

    @Column({type: 'decimal'})
    public total: number

    @ManyToOne(() => Pedido, (pedido) => pedido.pedidoProducto)
    public pedido!: Pedido

    @ManyToOne(() => Producto, (producto) => producto.pedidoProducto)
    public producto!: Producto
}