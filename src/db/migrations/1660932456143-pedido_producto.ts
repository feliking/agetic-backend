import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class pedidoProducto1660932456143 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'pedido_producto',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: "increment",
                    },
                    {
                        name: 'pedidoId',
                        type: 'int'
                    },
                    {
                        name: 'productoId',
                        type: 'int'
                    },
                    {
                        name: 'quantity',
                        type: 'int'
                    },
                    {
                        name: 'total',
                        type: 'decimal'
                    }
                ],
                foreignKeys: [
                    {
                        name: "Pedido",
                        referencedTableName: "pedidos",
                        referencedColumnNames: ["id"],
                        columnNames: ["pedidoId"],
                        onDelete: "CASCADE",
                        onUpdate: "CASCADE",
                    },
                    {
                        name: "Producto",
                        referencedTableName: "productos",
                        referencedColumnNames: ["id"],
                        columnNames: ["productoId"],
                        onDelete: "CASCADE",
                        onUpdate: "CASCADE",
                    },
                ]
            },)
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("pedido_producto")
    }

}
