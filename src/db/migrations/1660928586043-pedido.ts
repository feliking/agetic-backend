import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class pedido1660928586043 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'pedidos',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: "increment",
                    },
                    {
                        name: 'fecha',
                        type: 'varchar'
                    },
                    {
                        name: 'total',
                        type: 'decimal'
                    }
                ],
                /* foreignKeys: [
                    {
                        name: "Pedidos",
                        referencedTableName: "pedido_producto",
                        referencedColumnNames: ["pedidoId"],
                        columnNames: ["id"]
                    }
                ] */
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("pedidos")
    }

}
