import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm"

export class producto1660928250024 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'productos',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: "increment",
                    },
                    {
                        name: 'nombre',
                        type: 'varchar'
                    },
                    {
                        name: 'fabricante',
                        type: 'varchar'
                    },
                    {
                        name: 'origen',
                        type: 'varchar'
                    },
                    {
                        name: 'fecha_caducidad',
                        type: 'date'
                    },
                    {
                        name: 'precio',
                        type: 'decimal'
                    }
                ],
                /* foreignKeys: [
                    {
                        name: "Producto",
                        referencedTableName: "pedido_producto",
                        referencedColumnNames: ["productoId"],
                        columnNames: ["id"]
                    }
                ] */
            },)
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("productos")
    }

}
