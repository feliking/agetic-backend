import { MigrationInterface, QueryRunner } from "typeorm"

export class seeder1660960541753 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("INSERT INTO public.productos (nombre, fabricante, origen, fecha_caducidad, precio) VALUES('REDMI NOTE 11 PRO PLUS', 'XIAOMI', 'CHINA', '2025-09-19', 2700);")
        await queryRunner.query("INSERT INTO public.productos (nombre, fabricante, origen, fecha_caducidad, precio) VALUES('GALAXY S22', 'SAMSUNG', 'JAPON', '2022-08-19', 2200);")
        await queryRunner.query("INSERT INTO public.pedidos (fecha, total) VALUES('2022-08-19', 2700);")
        await queryRunner.query("INSERT INTO public.pedidos (fecha, total) VALUES('2022-08-13', 12500);")
        await queryRunner.query(`INSERT INTO public.pedido_producto ("pedidoId", "productoId", quantity, total) VALUES(1, 1, 1, 2700);`)
        await queryRunner.query(`INSERT INTO public.pedido_producto ("pedidoId", "productoId", quantity, total) VALUES(2, 2, 2, 4400);`)
        await queryRunner.query(`INSERT INTO public.pedido_producto ("pedidoId", "productoId", quantity, total) VALUES(2, 1, 3, 8100);`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
