import { DataSource } from "typeorm";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";

const options: PostgresConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'agetic',
  entities: [],
  synchronize: false,
  logging: true,
  migrations: ['db/migrations/*.ts']
}

export default new DataSource(options)