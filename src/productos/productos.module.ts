import { Module } from '@nestjs/common';
import { ProductosService } from './productos.service';
import { ProductosController } from './productos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Producto } from './entities/producto.entity';
import { PedidoProducto } from 'src/custom_entities/pedido_producto.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Producto, PedidoProducto])],
  controllers: [ProductosController],
  providers: [ProductosService]
})
export class ProductosModule {}
