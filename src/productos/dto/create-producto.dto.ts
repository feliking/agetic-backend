export class CreateProductoDto {
    nombre: string
    fabricante: string
    origen: string
    fecha_caducidad: string
}
