import { PedidoProducto } from "src/custom_entities/pedido_producto.entity";
import { Pedido } from "src/pedidos/entities/pedido.entity";
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: 'productos'})
export class Producto {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    nombre: string

    @Column()
    fabricante: string

    @Column()
    origen: string

    @Column({type: 'date'})
    fecha_caducidad: string

    @Column({type: 'decimal'})
    precio: number

    @OneToMany(() => PedidoProducto, pedidoProduct => pedidoProduct.producto)
    public pedidoProducto!: PedidoProducto[];

    @ManyToMany(() => Pedido, (pedido) => pedido.productos)
    @JoinTable({ name: 'pedido_producto', joinColumn: { name: 'productoId' }, inverseJoinColumn: { name: 'pedidoId' }, })
    pedidos: Pedido[]
}