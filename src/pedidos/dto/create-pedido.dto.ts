import { Producto } from "src/productos/entities/producto.entity"

export class CreatePedidoDto {
    fecha: string
    total: number
    manyRelations: Array<any>
}
