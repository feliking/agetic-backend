import { PedidoProducto } from "src/custom_entities/pedido_producto.entity";
import { Producto } from "src/productos/entities/producto.entity";
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: 'pedidos'})
export class Pedido {
    @PrimaryGeneratedColumn()
    id: number

    @Column({type: 'date'})
    fecha: string

    @Column({type: 'decimal'})
    public total: number

    @OneToMany(() => PedidoProducto, pedidoProduct => pedidoProduct.pedido)
    public pedidoProducto!: PedidoProducto[];

    @ManyToMany(() => Producto, (producto) => producto.pedidos, {
        cascade: true,
    })
    @JoinTable({ name: 'pedido_producto', joinColumn: { name: 'pedidoId' }, inverseJoinColumn: { name: 'productoId' }, })
    productos: Producto[]

}