import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PedidoProducto } from 'src/custom_entities/pedido_producto.entity';
import { Repository } from 'typeorm';
import { CreatePedidoDto } from './dto/create-pedido.dto';
import { UpdatePedidoDto } from './dto/update-pedido.dto';
import { Pedido } from './entities/pedido.entity';

@Injectable()
export class PedidosService {
  constructor(
    @InjectRepository(Pedido)
    private pedidosRepository: Repository<Pedido>,
    @InjectRepository(PedidoProducto)
    private pedidoProductoRepository: Repository<PedidoProducto>
  ) {}
  create(createPedidoDto: CreatePedidoDto) {
    try {
      const pedido = this.pedidosRepository.create(createPedidoDto);
      const storePedido = this.pedidosRepository.save(pedido);
      storePedido.then(value => {
        createPedidoDto.manyRelations.map(item => {
          const newPedidoProducto = new PedidoProducto();
          newPedidoProducto.pedidoId = value.id
          newPedidoProducto.productoId = item.id
          newPedidoProducto.quantity = item.quantity
          newPedidoProducto.total = item.total
          this.pedidoProductoRepository.save(newPedidoProducto)
        })
      })
      return null
    } catch (error) {
      throw error
    }
  }

  findAll() {
    return this.pedidosRepository.find({relations: ['productos.pedidoProducto', 'pedidoProducto.producto']});
  }

  findOne(id: number) {
    return this.pedidosRepository.findOneBy({id});
  }

  /* update(id: number, updatePedidoDto: UpdatePedidoDto) {
    return this.pedidosRepository.update(id, updatePedidoDto);
  } */

  async update(id: number, updatePedidoDto: UpdatePedidoDto) {
    try {
      const pedido = {
        fecha: updatePedidoDto.fecha,
        total: updatePedidoDto.total
      }
      await this.pedidosRepository.update(id, pedido);
      await this.pedidoProductoRepository.delete({pedidoId: id})
      for (let item of updatePedidoDto.manyRelations) {
        const newPedidoProducto = new PedidoProducto();
        newPedidoProducto.pedidoId = id
        newPedidoProducto.productoId = item.id
        newPedidoProducto.quantity = item.quantity
        newPedidoProducto.total = item.total
        this.pedidoProductoRepository.save(newPedidoProducto)
      }
      return null
    } catch (error) {
      throw error
    }
  }

  remove(id: number) {
    return this.pedidosRepository.delete(id);
  }
}
