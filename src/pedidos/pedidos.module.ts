import { Module } from '@nestjs/common';
import { PedidosService } from './pedidos.service';
import { PedidosController } from './pedidos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Pedido } from './entities/pedido.entity';
import { PedidoProducto } from 'src/custom_entities/pedido_producto.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Pedido, PedidoProducto])],
  controllers: [PedidosController],
  providers: [PedidosService]
})
export class PedidosModule {}
